
package com.mycompany.user;

import java.util.ArrayList;


public class UserService {
    private final ArrayList<User> userList;
    private int lastId = 0;
    
    public UserService() {
        userList = new ArrayList<User>();
        userList.add(new User(lastId, "HUM YAI", "HUM LEK", "Male", "CEO"));
    }
    public void addUser(User user){
        userList.add(user);
    }

    public ArrayList<User> getUserList() {
        return userList;
    }
    public User getUserById (int id){
       return userList.get(id);
        
    }
    public void deleteUser(User user){
        userList.remove(user);
    }

    int getSize() {
        return userList.size();
    }

    public int getLastId() {
        return lastId;
    }
    
    public void incrementLastId(){
        lastId++;
    }
    
}
